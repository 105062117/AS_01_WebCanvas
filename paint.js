var canvas = document.getElementById('mycanvas');
var context = canvas.getContext('2d');

var radius = 5,count = -1;
var drag = false;
var erase = false, textmode = false, hasInput = false;
var rect = false, cir = false, tri = false;
var beginx, beginy;

//Mouse Position
function getPos(canvas, e) {
    var place = canvas.getBoundingClientRect();
    return {
        x: e.clientX - place.left ,
        y: e.clientY - place.top 
    };
}

//canvas modify with window size
canvas.width=window.innerWidth-200;
canvas.height=window.innerHeight-150;

context.fillStyle = "white";
context.fillRect( 0, 0, canvas.width, canvas.height);

window.onresize= function(){
    var image = context.getImageData(0,0,canvas.width, canvas.height);

    canvas.width=window.innerWidth-200;
    canvas.height=window.innerHeight-150;

    context.putImageData(image, 0, 0);
}

//Eraser
document.getElementById('eraser').addEventListener('click', function() {
    if(erase)canvas.style.cursor = "url('picture/pen.jpg'), auto";
    else canvas.style.cursor = "url('picture/eraser.jpg'), auto";
    erase=!erase;
}, false);

//Change color
var red =document.getElementById('red'),
    blue =document.getElementById('blue'),
    green =document.getElementById('green');

// Drawing function
var putpoint = function(e){
    let now = getPos(canvas, e);

    context.lineWidth = radius*2;
    if(drag && !rect && !cir && !tri){
        context.lineTo(e.offsetX, e.offsetY);
        context.strokeStyle = (erase)?"rgba(255,255,255,1)":"rgba("+red.value+","+green.value+","+blue.value+",1)";
        context.stroke();
        context.beginPath();
        context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        context.fillStyle = (erase)?"rgba(255,255,255,1)":"rgba("+red.value+","+green.value+","+blue.value+",1)";
        context.fill();
        context.beginPath();
        context.moveTo(e.offsetX, e.offsetY);
    }
    else{
        context.strokeStyle ="rgba("+red.value+","+green.value+","+blue.value+",1)";
        context.lineWidth= textsize.value;

        if(drag && rect)  shapeR(now.x, now.y);
        else if(drag && cir) shapeC(now.x, now.y);
        else if(drag && tri) shapeT(now.x, now.y);
    }
}
var engage = function(e){
    let now = getPos(canvas, e);
   
    beginx = now.x;
    beginy = now.y;

    if(textmode) addInput(e.clientX, e.clientY, beginx, beginy);
    else{
        drag = true; 
        putpoint(e);
    }
}

var disengage = function(){
    if(drag) push();
    
    rect = false, cir = false, tri = false;
    erase = false, textmode = false, drag = false;

    canvas.style.cursor = "url('picture/pen.jpg'), auto";
    context.beginPath();
}

canvas.addEventListener('mousedown', engage, false);
canvas.addEventListener('mousemove', putpoint, false);
canvas.addEventListener('mouseup', disengage, false);
canvas.addEventListener('mouseleave', disengage, false);

//Painter size
var setsize = function(newsize){
    if(newsize<minsize)
        radius = minsize;
    else if(newsize > maxsize)
        radius = maxsize;
    else
        radius = newsize;
    
    context.lineWidth = radius*2;
    sizeshow.innerHTML = " "+radius+" px";
}

var minsize = 1,
    maxsize = 25,
    sizeshow = document.getElementById('pensize');
    
    document.getElementById('addsize').addEventListener('click', function() {
         setsize(radius+1);
    }, false);
    document.getElementById('subsize').addEventListener('click', function() { 
        setsize(radius-1); 
    } );

//reset function
document.getElementById('reset').addEventListener('click', function() {
    context.fillStyle = "white";
    context.fillRect( 0, 0, canvas.width, canvas.height);
    push();
}, false);


//Download
function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

document.getElementById('save').addEventListener('click', function() {
    downloadCanvas(this, 'mycanvas', Math.random().toString(36).substr(3, 11));
}, false);

//Text input
document.getElementById('textinput').addEventListener('click', function(e){
    textmode=!textmode;
    canvas.style.cursor = "url('picture/text.jpg'), auto";
},false);

function addInput(x, y, beginx, beginy) {
    textmode = false;

    var input = document.createElement('input');
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x)+ 'px';
    input.style.top = (y)+ 'px';
    input.style.fontSize = textsize.value+'px';
    input.style.border = '2px solid #000000';

    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, beginx, beginy);
        
        document.body.removeChild(this);
        
        hasInput = false;
        canvas.style.cursor = "url('picture/pen.jpg'), auto";
        push();
    }
}

var textsize = document.getElementById('textsize');
var fonttype = document.getElementById('font');

function drawText(txt, x, y) {
    context.textBaseline = 'top';
    context.textAlign = 'left';
    if(fonttype.value=="")
        context.font = textsize.value+"px Nadeem";
    else
        context.font = textsize.value+"px "+fonttype.value;
    
    context.fillStyle = "rgba("+red.value+","+green.value+","+blue.value+",1)";
    context.fillText(txt, x, y);
}

//upload
var imgupload = document.getElementById('upload');
imgupload.addEventListener('change' , paste, false);

function paste(e){
    var loader = new FileReader();
    loader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            context.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    loader.readAsDataURL(e.target.files[0]);
}

// Undo/Redo
var array = new Array();

document.getElementById('undo').addEventListener('click', undo);
document.getElementById('redo').addEventListener('click', redo);

function push(){
    count++;
    if(count<array.length) array.length = count;
    array.push(canvas.toDataURL());
}

function undo(){
    if(count > 0){
        count--;
        fillarray();
    }
}

function redo(){
    if(count < array.length){
        count++;
        fillarray();
    }
}
function fillarray(){
        var pic = new Image();
        pic.src = array[count];
        pic.onload = function(){
            context.drawImage(pic, 0, 0, canvas.width, canvas.height);
        } 
}

push();

//Shape
function change(){
    var pic = new Image();
    pic.src = array[count];
    context.drawImage(pic, 0, 0);
}

// Rectangle
document.getElementById('rect').addEventListener('click', function(){
    rect=!rect;
    if(rect)canvas.style.cursor = "url('picture/rect.jpg'), auto";
    else canvas.style.cursor = "url('picture/pen.jpg'), auto";
},false);

function shapeR(x, y){
    context.clearRect(0,0,canvas.width,canvas.height);
    change();
    context.strokeRect(beginx, beginy, x-beginx, y-beginy);
}

//Circle
document.getElementById('cir').addEventListener('click', function(){
    cir=!cir;
    if(cir)canvas.style.cursor = "url('picture/circle.jpg'), auto";
    else canvas.style.cursor = "url('picture/pen.jpg'), auto";
},false);

function shapeC(x, y){
    context.clearRect(0,0,canvas.width,canvas.height);
    change();
    context.beginPath();
    context.arc(beginx, beginy, Math.sqrt(Math.pow(x-beginx, 2) + Math.pow(y-beginy, 2)), 0, Math.PI*2);
    context.stroke();
}

//Triangle
document.getElementById('tri').addEventListener('click', function(){
    tri=!tri;
    if(tri)canvas.style.cursor = "url('picture/tri.jpg'), auto";
    else canvas.style.cursor = "url('picture/pen.jpg'), auto";
},false);

function shapeT(x, y){
    context.clearRect(0,0,canvas.width,canvas.height);
    change();
    context.beginPath();
    context.moveTo(x,y);
    context.lineTo(beginx, beginy);
    context.lineTo(2*beginx-x, y);
    context.closePath();
    context.stroke();
}
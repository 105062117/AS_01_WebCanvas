# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


1. 使用一個getPos 取得mousedown座標 提供畫形狀能有一個基準點
2. 使用onresize 能夠隨著視窗大小 控制canvas的大小 而canvas在調整大小時 版面不會重置
3. mousedown會呼叫 engage 在那裡紀錄按下去的座標後 使用textmode判斷是否為輸入模式
4. mousemove會呼叫putpoint 在裡面能設計出畫畫的效果 並使用erase判斷是橡皮擦或畫筆 使用strokeStyle來輸入文字的rgb顏色
   以及rect cir tri判斷繪畫圖形的種類
5. 使用setsize控制畫筆粗細 使用兩個按鈕控制大小 並設計最大粗細 25px 最小 1px
6. reset 就是直接將整個canvas填滿白色
7. download使用tag<a>的屬性實作 並使用亂數函式設計出檔案名稱 便能重複存取
8. 輸入文字的部分 文字輸入框會在mousedown的位置出現 文字框的設定在addInput handleEnter是在偵測文字框內按下enter後必須要輸出     文字 drawtext是把文字的顏色與字型設定完後輸出 文字框會隨著輸入字的大小而有所調整
9. 運用paste將所選取的圖片貼在canvas上面
10. Push 是在控制每當完成一個步驟後 將內容存在array裡面 而undo redo fillarray則是控制現在應該輸出哪一張圖
11. change 是負責在 畫shape時 能夠看到當下會出現的形狀大小
12. shapeR shapeC shapeT分別將三個圖形畫出來 呼叫這三個函式的地方是putpoint 三個形狀的線條粗細也能由操控文字大小的位置控制
    而形狀的顏色也能做調整
13. 游標會隨著不同按鈕被按下時改變 而一開始的工具都是畫筆
14. 當游標移動到按鈕上時會有反白的效果 click時則會有按鈕被按下去的動畫
